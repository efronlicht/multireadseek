package multirseek

import (
	"errors"
	"fmt"
	"io"
	"slices"
	"sync"
)

// Reader is a Multi-Reader and Multi-Seeker.
// It reads and seeks from multiple io.ReadSeekers as if they were a single contiguous file
// created by concatenating the underlying io.ReadSeekers.
// It is _not_ safe for concurrent use.
// If the underlying readers are io.Closers, Close() will call Close() on all of them concurrently.
type Reader struct {
	rs     []io.ReadSeeker
	end    []int64 // cumulative end offset of each reader: end[n] = sum(len(rs[:n]))
	absOff int64   // absolute offset
}

// New creates a new MultiRSeeker from a list of io.ReadSeekers.
func New(rs ...io.ReadSeeker) (r *Reader, err error) {
	if len(rs) == 0 {
		return nil, fmt.Errorf("no readers")
	}
	end := make([]int64, len(rs))

	for i := range rs {
		end[i], err = rs[i].Seek(0, io.SeekEnd) // fast-forward the tape to the end to see how long it is
		if err != nil {
			return nil, fmt.Errorf("getting size of reader #%d (a %T): %w", i, rs[i], err)
		}
		if i > 0 {
			end[i] += end[i-1]
		}
		_, err = rs[i].Seek(0, io.SeekStart) // rewind the tape head
		if err != nil {
			return nil, fmt.Errorf("rewinding reader #%d (a %T): %w", i, rs[i], err)
		}

	}
	return &Reader{rs: rs, end: end}, nil
}

// Size returns the total size of all the underlying readers.
func (r *Reader) Size() int64 { return r.end[len(r.end)-1] }

// index of the reader that contains the current offset
func (r *Reader) idx() int { i, _ := slices.BinarySearch(r.end, r.absOff); return i }

// Seek sets the offset for the next Read to offset, interpreted according to whence: 0 means relative to the start of the file, 1 means relative to the current offset, and 2 means relative to the end.
// This will call Seek() on at most one underlying reader.
func (r *Reader) Seek(offset int64, whence int) (int64, error) {
	switch whence {
	case io.SeekStart:
		r.absOff = offset
	case io.SeekCurrent:
		r.absOff += offset
	case io.SeekEnd:
		r.absOff = r.Size() + offset
	}
	switch {
	case r.absOff < 0, r.absOff > r.Size():
		return r.absOff, fmt.Errorf("seeking to %d: out of bounds", offset)
	}
	// find the reader that contains the offset
	_, err := r.rs[r.idx()].Seek(r.relOff(), io.SeekStart)
	if err != nil {
		return r.absOff, fmt.Errorf("seeking to %d (relative offset %d in reader #%d, a %T): %w", r.absOff, r.relOff(), r.idx(), r.rs[r.idx()], err)
	}

	return r.absOff, nil
}

// relOff returns the relative offset of the current reader.
func (r *Reader) relOff() int64 { return r.absOff - r.end[r.idx()] }

// Read reads from the underlying readers as if they were a single contiguous file.
// Each call to Read calls Read() at most once on at most one of the underlying readers,
// and Seek() on at most one of the underlying readers (but NOT the same one as the Read() call, if any).
func (r *Reader) Read(p []byte) (n int, err error) {
	i := r.idx()
	if i >= len(r.rs) { // end of all files
		return 0, io.EOF
	}
	// each read call will read from at most one reader.
	// that's OK, since you can use io.ReadFull to read from a MultiRSeeker
	n, err = r.rs[i].Read(p)

	r.absOff += int64(n)
	if r.relOff() < r.end[i] { // the read didn't spill over to the next reader
		return n, err
	}
	i++ // spill over to the next reader

	if i >= len(r.rs) { // end of all files
		return n, io.EOF
	}
	// rewind the tape head of the next file to prepare for the next read
	_, err = r.rs[i].Seek(0, io.SeekStart)
	if err != nil {
		return n, fmt.Errorf("rewinding reader #%d (a %T): %w", i, r.rs[i], err)
	}
	return n, err
}

// Close closes all the underlying readers that are also io.Closers.
// It is undefined behavior to call Close() more than once, or to call Read() or Seek() after Close().
// So don't do that.
func (r *Reader) Close() error {
	errs := make([]error, len(r.rs))
	var wg sync.WaitGroup
	for i := range r.rs {
		if c, ok := r.rs[i].(io.Closer); ok {
			wg.Add(1)
			go func() {
				defer wg.Done()
				if err := c.Close(); err != nil {
					errs[i] = fmt.Errorf("close reader #%d: (a %T): %w", i, r, err)
				}
			}()
		}
	}
	wg.Wait()
	return errors.Join(errs...)
}
